const express = require('express')


//Created an application using express
//In layman's term, app is our server

const app = express();
const port = 4000


//Middlware

app.use(express.json());
app.use(express.urlencoded({extended:true}))


let users = []

//Routes/endpoint
//http:localhost:4000/

//get
app.get("hello",(req,res) =>{
res.send("Hello from the /hello endpoint!")
})


//post

app.post("/hello",(req,res) => {

//req.body contains the contents/data of the request body
//
res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)

})





app.listen(port,()=> console.log(`Server running at port:${port}`))


//Create a Post route to register a user.

app.post("/signup", (req,res) => {
	console.log(req.body);
	//validation
	//If contents of the req.body with the property username and password is not empty, then push the data to the users array. else, please input both username and password
	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered`)
	} else{
		res.send("Please input BOTH username and password")
	}
})


//This route expects to recieve a PUT request at the URI "/change-password"
//put
//This will update the password or a user that matches the information provided the client

app.put("/change-password", (req,res) => {


for(let i = 0; i < users.length;i++)

	if(req.body.username === users[i].username){
    //change the password of the user found by the loop into the provided in the client.

    users[i].password = req.body.password

   //message response
    

    message =`User ${req.body.username}'s password has been updated`;

   //breaks out of the loop once a user matches the username provided
    break;

}else {

	message = "User does not exist"
}

res.send(message)


})

